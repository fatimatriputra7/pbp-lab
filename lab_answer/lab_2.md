1. Apakah perbedaan antara JSON dan XML?
- JSON menyimpan data-data yang diterima kedalam sebuah map yang membentuk key dan value sehingga semua datanya terlihat lebih rapi dan mudah dipahami. XML menyimpan data dengan membuat tree structure sehingga konfigurasi menjadi lebih dinamis. XML sudah ada jauh lebih lama dibanding JSON namun format penukaran data semakin berkembang sehingga JSON jauh lebih ringan dibanding XML dan lebih mudah untuk diurai oleh komputer. JSON mendukung penyandian menggunakan UTF dan ASCII, sedangkan XML mendukung kode dengan UTF-8 dan UTF-16. Struktur data XML juga lebih rentan terkena serangan karena entitas eksternal dan validasi DTD diaktifkan secara default, sedangkan JSON dapat dipastikan aman hampir sepanjang waktu.

2. Apakah perbedaan antara HTML dan XML?
- HTML lebih memfokuskan pada penyajian data sedangkan XML fokus pada aspek transfer data. XML memiliki aturan yang lebih ketat dibanding HTML. XML tidak memperbolehkan adanya error, dapat mempertahankan spasi dan juga case sensitive. Dilain tangan, HTML berbanding tebalik dengan XML karena tidak dapat mempertahankan spasi, dapat mengabaikan error-error minor dan case insensitive. Walaupun begitu, HTML juga memiliki beberapa "fitur" yang membuat HTML lebih menarik karena HTML dapat merubah warna, objek dan tata letak data sedangkan XML lebih simpel dan memiliki format tersendiri.

Referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html