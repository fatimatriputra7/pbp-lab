from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_2.models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'index_lab2.html', response)

def xml(request):
    notes = Note.objects.all()
    xml_notes = serializers.serialize('xml', notes)
    return HttpResponse(xml_notes, content_type="application/xml")

def json(request):
    notes = Note.objects.all()
    json_notes = serializers.serialize('json', notes)
    return HttpResponse(json_notes, content_type="application/json")