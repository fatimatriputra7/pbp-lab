import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Add New Discussion',
        theme: ThemeData(
          primaryColor: Colors.black,
          scaffoldBackgroundColor: const Color(0xFF7ABECB),
          fontFamily: "Source Sans Pro",
        ),
        home: const MyDrawer());
  }
}

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Covid Cares",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: const MyForm(),
      drawer: Drawer(
        child: ListView(
          padding: const EdgeInsets.all(5.0),
          children: [
            ListTile(
              title: const Text('Home'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Forum'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ExpansionTile(
              title: const Text("Information Page"),
              children: <Widget>[
                ListTile(
                  title: const Text('COVID-19'),
                  onTap: () {
                    // close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Vaccine'),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Nearest Pharmacies and Medicines'),
                  onTap: () {
                    Navigator.pop(context);
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class MyForm extends StatelessWidget {
  const MyForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MyCostumForm();
  }
}

class MyCostumForm extends StatefulWidget {
  const MyCostumForm({Key? key}) : super(key: key);

  @override
  MyCostumFormState createState() {
    return MyCostumFormState();
  }
}

class MyCostumFormState extends State<MyCostumForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
          key: _formKey,
          child: Container(
            decoration: BoxDecoration(
                color: const Color(0xFF74cfbf),
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    blurRadius: 10,
                    spreadRadius: 3,
                  ),
                ]),
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  'Add New Discussion',
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text("Title"),
              ),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    color: Colors.white,
                    width: 250,
                    height: 40,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Field cannot be empty";
                          }
                          return null;
                        }),
                  )),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text("Message"),
              ),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    color: Colors.white,
                    width: 250,
                    height: 40,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Field cannot be empty";
                          }
                          return null;
                        }),
                  )),
              Padding(
                padding: const EdgeInsets.all(10.0),
                // ignore: deprecated_member_use
                child: RaisedButton(
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: const Color(0xFFF7BA5B),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              )
            ]),
          )),
    );
  }
}
