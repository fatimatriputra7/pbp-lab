import 'package:flutter/material.dart';

// Referensi:
// contoh lab_6 (instructions)
// https://api.flutter.dev/flutter/material/Card-class.html
// https://docs.flutter.dev/cookbook/forms/validation
// https://flutterforyou.com/how-to-create-a-horizontal-line-in-flutter/

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Forum Discussion',
        theme: ThemeData(
          primaryColor: Colors.black,
          scaffoldBackgroundColor: const Color(0xFF7ABECB),
          fontFamily: "Source Sans Pro",
        ),
        home: const MyDrawer());
  }
}

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Covid Cares",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: const ForumPage(),
      drawer: Drawer(
        child: ListView(
          padding: const EdgeInsets.all(5.0),
          children: [
            ListTile(
              title: const Text('Home'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Forum'),
              onTap: () {
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ExpansionTile(
              title: const Text("Information Page"),
              children: <Widget>[
                ListTile(
                  title: const Text('COVID-19'),
                  onTap: () {
                    // close the drawer
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Vaccine'),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Nearest Pharmacies and Medicines'),
                  onTap: () {
                    Navigator.pop(context);
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class ForumPage extends StatelessWidget {
  const ForumPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 20.0),
            child: SizedBox(
              width: 145,
              height: 35,
              child: ElevatedButton(
                child: const Text("Add A Discussion"),
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => const MyForm()));
                },
                style: TextButton.styleFrom(
                    backgroundColor: const Color(0xFFF7BA5B),
                    primary: Colors.white),
              ),
            )),
        Padding(
          padding: const EdgeInsets.only(right: 10.0, left: 10.0),
          child: SizedBox(
            width: 700,
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      '1st discussion',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text('by fatima | Nov. 17, 2021, 03:47 p.m.'),
                  ),
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      'first forum text!!!',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Text('Reply'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: const Color(0xFF74cfbf),
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Edit'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.grey[600],
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Delete'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.red, primary: Colors.white),
                      ),
                      const SizedBox(width: 10),
                    ],
                  ),
                  const Divider(
                    color: Colors.grey,
                    height: 20,
                    thickness: 0.5,
                    indent: 10,
                    endIndent: 10,
                  ),
                  Container(
                    color: Colors.grey[300],
                    width: 590,
                    margin: const EdgeInsets.only(bottom: 10),
                    child: const ListTile(
                      title: Text(
                        'fatima - Nov. 18, 2021, 06:24 p.m.',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 12),
                      ),
                      subtitle: Text('wow ada komen'),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 10.0, left: 10.0),
          child: SizedBox(
            width: 700,
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      '2nd post',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text('by fatima | Nov. 18, 2021, 04:01 p.m.'),
                  ),
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      'jika bisa begini, bisa begitu',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Text('Reply'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: const Color(0xFF74cfbf),
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Edit'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.grey[600],
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Delete'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.red, primary: Colors.white),
                      ),
                      const SizedBox(width: 10),
                    ],
                  ),
                  const Divider(
                    color: Colors.grey,
                    height: 20,
                    thickness: 0.5,
                    indent: 10,
                    endIndent: 10,
                  ),
                  Container(
                    color: Colors.grey[300],
                    width: 590,
                    margin: const EdgeInsets.only(bottom: 10),
                    child: const ListTile(
                      title: Text(
                        'Add a Reply',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 12),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class MyForm extends StatelessWidget {
  const MyForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Covid Cares",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: const MyCostumForm(),
    );
  }
}

class MyCostumForm extends StatefulWidget {
  const MyCostumForm({Key? key}) : super(key: key);

  @override
  MyCostumFormState createState() {
    return MyCostumFormState();
  }
}

class MyCostumFormState extends State<MyCostumForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
          key: _formKey,
          child: Container(
            color: const Color(0xFF74cfbf),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text("Title"),
              ),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    color: Colors.white,
                    width: 250,
                    height: 40,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Field cannot be empty";
                          }
                          return null;
                        }),
                  )),
            ]),
          )),
    );
  }
}
